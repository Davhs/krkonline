<?php

use Illuminate\Database\Seeder;

class SongsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('songs')->truncate();

        DB::table('songs')->insert([
            [
                'judul_lagu' => 'House Music DJ Ivan (MORENA)',
                'path' => 'DBSong7/Barat/Barat#House Music#DJ IVAN TUKUL#DUGEM NONSTOP  2013 ( MORENA ) 2jam#Right.mp4'
            ],
            [
                'judul_lagu' => 'House Music DJ Ivan INDONESIA HOUSE MUSIC',
                'path' => 'DBSong7/Barat/Barat#House Music#DJ Ivan#INDONESIA HOUSE MUSIC DUGEM  2013 Nonstop#Right.mp4'
            ],
            [
                'judul_lagu' => 'House Music DJ Kaisar Ivan Dangdut Remix 2013 Nonstop.mp4',
                'path' => 'DBSong7/Barat/Barat#House Music#DJ Kaisar Ivan#Dangdut Remix 2013 Nonstop#Right.mp4'
            ],
            [
                'judul_lagu' => 'House Music DJ NN (BARA-BERE) Nonstop',
                'path' => 'aDBSong7/Barat/Barat#House Music#DJ NN  BATAM#BATAM 2014_(BARA-BERE) Nonstop#Right.mp4'
            ],
        ]);
    }
}
