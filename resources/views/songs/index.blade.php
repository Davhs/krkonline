@extends('layouts.app')
@section('active1', 'active')
@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">All Song</div>
                        <div class="panel panel-body">
                        </div>
                        <ul class="list-group">
                            @if($songs->isEmpty())
                                <button type="button" class="list-group-item">
                                    <p align="center">Nothing!</p>
                                </button>
                            @else
                                @foreach($songs as $song)
                                  <a href="{{ route('song.show', $song->id) }}" class="list-group-item">
                                    {{-- <span class="badge"><i class="fa fa-trash"></i></span> --}}
                                    <i class="fa fa-music"> {{ $song->judul_lagu  }}</i></a>
                                @endforeach
                            @endif
                        </ul>
                    {{-- <div class="panel-footer"></div> --}}
                </div>
                {!! $songs->render() !!}
            </div>
        </div>
@endsection
