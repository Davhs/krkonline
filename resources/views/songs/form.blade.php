@extends('layouts.app')
@section('active1', 'active')
@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Add New Song</div>
                    <div class="panel panel-body">
                        {!! Form::model($songs, [
                           'method' => $songs->exists ? 'put' : 'post', 'files' => true,
                           'route' => $songs->exists ? ['song.update', $songs->id] : ['song.store']
                        ]) !!}

                        <div class="form-group">
                            {!! Form::label('Judul Lagu') !!}
                            {!! Form::text('judul', null, ['class' => 'form-control required']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Lagu') !!}
                            {!! Form::file('nama_default', ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="panel-footer">
                        {!! Form::submit($songs->exists ? 'Update' : 'Save', ['class' => 'btn btn-primary']) !!}
                        <a class="btn btn-default" href="{{ route('song.index')  }}">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
@endsection
