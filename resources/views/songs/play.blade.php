@extends('layouts.app')
@section('active1', 'active')
@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-music"> {{$songs->judul_lagu}}</i></div>
                    <div class="panel-body">
                        <div class="embed-responsive embed-responsive-16by9">
                            <video class="embed-responsive-item" controls>
                                <source src="{{ asset('songs/'.$songs->path) }}">
                            </video>
                        </div>
                    </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Songs</div>
                <ul class="list-group">
                      @foreach($song as $songs)
                        <a href="{{ route('song.show', $songs->id) }}" class="list-group-item">
                          {{-- <span class="badge"><i class="fa fa-trash"></i></span> --}}
                          <i class="fa fa-music"> {{ $songs->judul_lagu  }}</i></a>
                      @endforeach
                </ul>
            </div>
            {!! $song->render() !!}
        </div>
    </div>
@endsection
