<?php

namespace App\Http\Controllers;

use App\Song;
use Illuminate\Http\Request;

use Validator;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class SongController extends Controller
{
    protected $songs;

    public function __construct(Song $songs)
    {
        $this->middleware('auth');

        $this->songs = $songs;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $songs = DB::table('songs')->paginate(5);

        return view('songs.index', compact('songs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Song $songs)
    {
        return view('songs.form', compact('songs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreRequestSong $request)
    {
      $data = $request->all();

      if ($request->hasFile('nama_default')) {

          $video = $request->file('nama_default');
          $jalur = './songs';
          $namaVideo = $video->getClientOriginalName();
          $ekstensi = $video->getClientOriginalExtension();
          $namaBaruVideo = rand(11111,99999).$namaVideo;

          if ($ekstensi == 'mp4') {

              $upload = $request->file('nama_default')->move($jalur, $namaBaruVideo);

              if ($upload) {

                  $data['nama_default'] = $namaBaruVideo;

                  $this->songs->create($data);
                  return redirect(route('song.index'))->with('status', 'Video Berhasil Di Upload.');
              }
          }
          else {
            return redirect(route('song.index'))->with('status', 'Video Gagal Di Upload, Pastikan extensi video mp4.');
          }
      }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $songs = DB::table('songs')->findOrFail($id);
        $songs = $this->songs->findOrFail($id);
        $song = DB::table('songs')->paginate(5);

        return view('songs.play', compact('songs', 'song'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
